package com.example.calculator;




import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
 
public class MainActivity extends Activity {
 
    Button button0 , button1 , button2 , button3 , button4 , button5 , button6 ,
            button7 , button8 , button9 , buttonAdd , buttonSub , buttonDivision ,
            buttonMul , button10 , buttonC , buttonEqual ;
    ImageButton backS;
 
   TextView input,output;
 
    float mValueOne , mValueTwo ;
 
    boolean mAddition , mSubtract ,mMultiplication ,mDivision ;
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
 
        button0 = (Button) findViewById(R.id.btnZero);
        button1 = (Button) findViewById(R.id.btnOne);
        button2 = (Button) findViewById(R.id.btnTwo);
        button3 = (Button) findViewById(R.id.btnThree);
        button4 = (Button) findViewById(R.id.btnFour);
        button5 = (Button) findViewById(R.id.btnFive);
        button6 = (Button) findViewById(R.id.btnSix);
        button7 = (Button) findViewById(R.id.btnSeven);
        button8 = (Button) findViewById(R.id.btnEight);
        button9 = (Button) findViewById(R.id.btnNine);
        button10 = (Button) findViewById(R.id.btnDecimal);
        buttonAdd = (Button) findViewById(R.id.btnAdd);
        buttonSub = (Button) findViewById(R.id.btnSubtract);
        buttonMul = (Button) findViewById(R.id.btnMultiply);
        buttonDivision = (Button) findViewById(R.id.btnDivide);
        buttonC = (Button) findViewById(R.id.btnClear);
        buttonEqual = (Button) findViewById(R.id.btnEquals);
        backS =  (ImageButton) findViewById(R.id.btnBack);
 input = (TextView) findViewById(R.id.txtInput);
output = (TextView) findViewById(R.id.txtoutput);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText(input.getText()+"1");
            }
        });
 
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText(input.getText()+"2");
            }
        });
 
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText(input.getText()+"3");
            }
        });
 //
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText(input.getText()+"4");
            }
        });
 
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText(input.getText()+"5");
            }
        });
 
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText(input.getText()+"6");
            }
        });
 
        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText(input.getText()+"7");
            }
        });
 
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText(input.getText()+"8");
            }
        });
 
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText(input.getText()+"9");
            }
        });
 
        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText(input.getText()+"0");
            }
        });
 
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
 
                if (input == null){
                    output.setText("");
                }else {
                    mValueOne = Float.parseFloat(input.getText() + "");
                    output.setText(input.getText()+" + ");
                    input.setText("");
                    mAddition = true;
                   
                }
            }
        });
 
        buttonSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mValueOne = Float.parseFloat(input.getText() + "");
               
                output.setText(input.getText()+" - ");
                input.setText("");
                mSubtract = true ;
              
            }
        });
 
        buttonMul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
         
                mValueOne = Float.parseFloat(input.getText() + "");
               
                output.setText(input.getText()+" * ");
                input.setText("");
                mMultiplication = true ;
                
            }
        });
 
        buttonDivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mValueOne = Float.parseFloat(input.getText()+"");
                
                output.setText(input.getText()+" / ");
                input.setText("");
                mDivision = true ;
               
            }
        });
 
        buttonEqual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mValueTwo = Float.parseFloat(input.getText() + "");
 
                float result =0;
                if (mAddition == true){
                	result =mValueOne + mValueTwo ;
                    output.setText(output.getText()+String.valueOf(mValueTwo)+" = "+String.valueOf(result));
                    mAddition=false;
                }
 
 
                if (mSubtract == true){
                	result =mValueOne - mValueTwo ;
                	output.setText(output.getText()+String.valueOf(mValueTwo)+" = "+String.valueOf(result));
                    mSubtract=false;
                }
 
                if (mMultiplication == true){
                	result =mValueOne * mValueTwo ;
                	output.setText(output.getText()+String.valueOf(mValueTwo)+" = "+String.valueOf(result));
                    mMultiplication=false;
                }
 
                if (mDivision == true){
                	result =mValueOne / mValueTwo ;
                	output.setText(output.getText()+String.valueOf(mValueTwo)+" = "+String.valueOf(result));
                    mDivision=false;
                }
            }
        });
 
        buttonC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText("");
                output.setText("");
            }
        });
backS.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		String temp = (String) input.getText() ;
		String newText = temp.substring(0,temp.length()-1);
		input.setText(newText);
	}
});
        button10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText(input.getText()+".");
            }
        });
    }
 
 
 

	
}
